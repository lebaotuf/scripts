#!/bin/sh

bring_up_frame() {
    if [ -z "$DISPLAY" ]; then
	emacsclient -tty
    elif [ -n "$_NEW_FRAME" ]; then
	emacsclient -nc
    elif [ "$(emacsclient -e '(terminal-name)' | tr -d '"')" = "$DISPLAY" ] && \
	 [ -z "$_NEW_FRAME" ]; then
	emacsclient -e '(progn (raise-frame) (select-frame-set-input-focus (selected-frame)))'
    else
	emacsclient -nc
    fi
}

open_in_frame() {
    local file_args emacs_args has_frame on_tty filename

    emacs_args="-n"

    if [ -z "$DISPLAY" ]; then
	emacs_args="-tty"
	on_tty=1
    elif [ "$(emacsclient -e '(terminal-name)' | tr -d '"')" = "$DISPLAY" ]; then
	has_frame=1
    fi

    if [ -n "$_NEW_FRAME" ]; then
	file_args="${@:2}"
	if [ -z "$on_tty" ]; then
	    emacs_args=$emacs_args" -c"
	fi
    else
	if [ -z "$has_frame" ]; then
	    emacsclient -nc
	fi
	file_args="$@"
    fi

    if [ -z $on_tty ]; then
	emacsclient -e '(progn (raise-frame) (select-frame-set-input-focus (selected-frame)))'
    fi

    for i in $file_args ; do
	# TRAMP needs absolute path
	if [ -e "$i" ]; then
	    filename="$(realpath $i)"
	else
	    filename="$i"
	fi
	emacsclient $emacs_args -e '(find-file "'$_SE$filename'")'
    done
}

unset _SE

if [ $0 = "/usr/local/bin/sudoec" ]; then
    _SE='/sudo::'
fi

if [ $# -ne 0 ] && [ $1 = "-c" ]; then
    _NEW_FRAME=1
fi

if [ $# -eq 0 ] || [ -n "$_NEW_FRAME" ] && [ $# -eq 1 ]; then
    bring_up_frame
    exit 0
fi

open_in_frame $@
