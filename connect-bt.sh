#!/usr/bin/env zsh

RECV_DEV="00:11:67:80:30:A6"

print_usage(){
	echo "Usage: $1 [-v] command"
	echo ""
	echo "	-v:		verbose output"
	exit 1
}

check(){
	conns="$(dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/$DBUS_DEV org.freedesktop.DBus.Properties.Get string:'org.bluez.Device1' string:'Connected' |& grep --color=auto 'boolean true')"
	profile="$(pacmd list-sinks | head -n1 | grep '1 sink')"
	if [[ ( "$conns" != '' ) && ( "$profile" == '' ) ]]; then
		echo 0
	else
		echo 1
	fi
}

info(){
	echo -e 'show\nexit' | bluetoothctl
}

power_on(){
	#Wait a minute
	sleep 1
	cstr='power on\nexit'
	[[ "$verbose" == 1 ]] && echo "Power on" && echo 'Command: $ bluetoothctl' && echo "$cstr"
	echo -e "$cstr" | bluetoothctl	&> /dev/null


	#Check power state
	if [[ "$(info | grep Powered | awk '{print $2}')" == "yes" && "$verbose" == 1 ]]; then
		echo "Powering on done"
	elif [[ "$(info | grep Powered | awk '{print $2}')" != "yes" ]]; then
		[[ "$verbose" == 1 ]] && echo "Powering on failed, retrying"
		power_on
	fi
}

unblock_bt(){
	state=$(rfkill list | grep 'hci' -A1 | tail -n1 | awk '{print $3}')
	rfkill_devnum=$(rfkill list | grep 'hci'| awk -F':' '{print $1}')

	if [[ "$state" == "yes" && "$verbose" == 1 ]]; then
		echo "Rfkill device: $rfkill_devnum"
		echo "Blocked: $state"
		echo "Bluetooth device is soft blocked, unblocking it..."
		rfkill unblock "$rfkill_devnum"
	elif [[ "$state" == "yes" && "$verbose" == 0 ]]; then
		rfkill unblock "$rfkill_devnum"
	fi
}

conn(){
	if [[ "$(check)" == 0 ]]; then
		echo "Already connected"
		exit 0
	fi

	unblock_bt
	power_on
	cstr='connect '$RECV_DEV'\nexit'
	[[ "$verbose" == 1 ]] && echo "Connecting:" && echo 'Command: $ bluetoothctl' && echo "$cstr"
	echo -e  "$cstr" | bluetoothctl &> /dev/null

	sleep 5
	cards=("${=$(pacmd list-cards | grep -B 1 'bluez_card' | awk '/index/ {if($2 ~ /^[0-9]*$/) print $2}')}")
	if [[ "${cards[@]}" != "" ]]; then
		pacmd set-card-profile "${cards[-1]}" a2dp_sink
	fi
	if [[ "$(check)" != 0 ]]; then
		[[ "$verbose" == 1 ]] && echo "Connection failed, retrying"
		conn
	else
		[[ "$verbose" == 1 ]] && echo "Connecting done"
	fi
}
disconn(){
	echo -e 'disconnect '$RECV_DEV'\nexit' | bluetoothctl &> /dev/null
}

DBUS_DEV="dev_${RECV_DEV//:/_}"

if [[ "$1" == "-v" ]]; then
	verbose=1
	command=$2
elif [[ "$1" != "" ]]; then
	verbose=0
	command=$1
else
	print_usage "$0"
fi

case  $command in
	"check")
		check
		;;
	"info")
		info
		;;
	"conn")
		conn
		;;
	"disconn")
		disconn
		;;
	"reconn")
		disconn
		sleep 5
		conn
		;;
	*)
		print_usage "$0"
		;;
esac
[[ "$verbose" == 1 ]] && echo "Operation finished"
exit 0
