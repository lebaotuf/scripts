#!/bin/sh

# Check the status of MOC (should be running as a systemd user service)
mocp_stat(){
	MOCP_STATUS="$(mocp -i | grep 'State' | awk '{print $2}')"
	if [[ $MOCP_STATUS == "PLAY" ]]; then
                MS="1"
        else
                MS=""
        fi
}

# Check the status of spotify (if it's running)
spotify_stat(){
	if [[ $(dbus-send --print-reply --session --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:'org.mpris.MediaPlayer2.Player' string:'PlaybackStatus' &> /dev/null || true) -eq 0  ]]; then
		SPOTIFY_STATUS="$(dbus-send --print-reply --session --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:'org.mpris.MediaPlayer2.Player' string:'PlaybackStatus' | grep 'variant' | awk '{print $3}' | sed 's/"//g')"
	else
		SS=""
		return
	fi

	if [[ $SPOTIFY_STATUS == "Playing" ]]; then
		SS="1"
	else
		SS=""
	fi
}

mocp_stat
spotify_stat

# Check if both playing or both stopped and no player saved
if [[ (( -n $SS ) && ( -n $MS )) || (( -z $SS ) && ( -z $MS ) && ! ( -f /tmp/player )) ]]; then
	exit 0
fi

# If none of them playing, start the player saved in the temporary file
if [[ (( -z $SS ) && ( -z $MS)) ]]; then
	if [[ $( cat /tmp/player) == "spotify" ]]; then
		dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause
	else
		mocp -G
	fi
	exit 0
fi

#If spotify is playing, save it and pause it
if [[ ( -n $SS ) ]]; then
	echo "spotify" > /tmp/player
	dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause
	exit 0
fi

#If MOC is playing, save it and pause it
if [[ ( -n $MS ) ]]; then
        echo "mocp" > /tmp/player
	mocp -G
	exit 0
fi
