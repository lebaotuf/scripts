blctl
=====

backlight control written in C

### Build

$ make

You might need to change the path in src/main.rs to the appropriate directory under /sys/class/backlight.

### Usage

Run it as root:

\# ./blctl +/-

### Installation

\# make install
