#include <stdio.h>
#include <string.h>
#include <errno.h>

#define BRIGHTNESS_PATH "/sys/class/backlight/intel_backlight/brightness"
#define MAX_BRIGHTNESS_PATH "/sys/class/backlight/intel_backlight/max_brightness"
#define LOW_THRESHOLD 2

int get_value(const char *path)
{
	FILE *fvalue;
	int value, fscanf_ret;
	fvalue = fopen(path, "r");
	if(fvalue == NULL){
		int errsv = errno;
		fprintf(stderr, "fopen: %s: %s\n", path, strerror(errsv));
		return -1;
	}
	fscanf_ret = fscanf(fvalue, "%d", &value);
	if(fscanf_ret < 1){
		return -1;
	}
	fclose(fvalue);
	return value;
}

int set_value(const char *path, int value)
{
	FILE *fvalue;
	fvalue = fopen(path, "w");
	if(fvalue == NULL){
		int errsv = errno;
		fprintf(stderr, "fopen: %s: %s\n", path, strerror(errsv));
		return -1;
	}
	fprintf(fvalue, "%d\n", value);

	fclose(fvalue);
	return 0;
}

int set_backlight(int change)
{
	int max_brightness, brightness, ret;

	max_brightness = get_value(MAX_BRIGHTNESS_PATH);
	brightness = get_value(BRIGHTNESS_PATH);

	if(max_brightness < 0 || brightness < 0){
		return -1;
	}

	/*printf("brightness: %d, max_brightness: %d, change: %d\n", brightness, max_brightness, change);*/

	if(change+brightness >= max_brightness){
		brightness = max_brightness;
	} else if(change+brightness <= LOW_THRESHOLD){
		brightness = LOW_THRESHOLD;
	} else {
		brightness+=change;
	}
	ret = set_value(BRIGHTNESS_PATH, brightness);
	if(ret < 0){
		return -1;
	}

	return 0;
}

int main(int argc, char **argv)
{
	int ret=-1;
	if(argc != 2){
		fprintf(stderr, "%s\n", "Missing argument");
		return -1;
	}
	if(strlen(argv[1]) != 1){
		return -1;
	}
	if(!strncmp(argv[1],"+",1)){
		ret=set_backlight(10);
	}
	if(!strncmp(argv[1],"-",1)){
		ret=set_backlight(-10);
	}
	if(ret != 0){
		return ret;
	}
	return 0;
}
