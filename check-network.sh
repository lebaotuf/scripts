#!/bin/sh

if [[ $(netctl status ethernet-dhcp | grep inactive) != "" ]]; then
	exit 0
else
	iface=$(netctl status ethernet-dhcp | grep -E '└─[0-9]* dhcpcd' | awk '{print $NF}')
fi

if [[ $(ip link show $iface | grep ' DOWN ') != ""  ]]; then
	netctl start ethernet-dhcp
fi

ping -c1 192.168.0.1 > /dev/null 2>&1

if [[ $? != 0 ]]; then
	netctl restart ethernet-dhcp
fi

exit 0
